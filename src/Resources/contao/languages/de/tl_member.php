<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_member']['library_name'] = ['Bibliothekname mit Zusatz', 'Hier kann frei der Name mit Zusatz vergeben werden.'];
$GLOBALS['TL_LANG']['tl_member']['library_id'] = ['Bibliothek', 'Wenn die Adressfelder leer sind, werden sie beim abspeichern automatisch mit dem Bibliotheksdaten befüllt.'];
$GLOBALS['TL_LANG']['tl_member']['state'] = ['Bundesland', 'Hier können Sie die Bibliothek einem Bundesland zuordnen.'];
$GLOBALS['TL_LANG']['tl_member']['job_position'] = ['Funktionsbezeichnung', 'Geben Sie hier an in welchem Bereich bzw welche Position dieses Mitglied bekleidet.'];
$GLOBALS['TL_LANG']['tl_member']['csvMemberExport'] = ['Mitglieder-Export', 'Geben Sie hier an in welchem Bereich bzw welche Position dieses Mitglied bekleidet.'];
$GLOBALS['TL_LANG']['tl_member']['button_exportcsv'] = 'CSV generieren';
$GLOBALS['TL_LANG']['tl_member']['h2_exportcsv'] = 'Mitglieder als CSV exportieren';

$GLOBALS['TL_LANG']['tl_member']['job_position_options'] = ['sonstige' => 'sonstige', 'Bibliotheksleitung' => 'Bibliotheksleitung', 'IT' => 'IT', 'Bibliotheksmitarbeiter' => 'Bibliotheksmitarbeiter'];
