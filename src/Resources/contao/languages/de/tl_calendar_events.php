<?php
$GLOBALS['TL_LANG']['tl_calendar_events']['teaser_legend'] = 'Teaser-Boxen';

$GLOBALS['TL_LANG']['tl_calendar_events']['teaserImageSRC'] = ['Teaser-Bild'];
$GLOBALS['TL_LANG']['tl_calendar_events']['teaserTitle'] = ['Teaser-Titel'];
$GLOBALS['TL_LANG']['tl_calendar_events']['teaserSubTitle'] = ['Teaser kurze Bemerkung'];
$GLOBALS['TL_LANG']['tl_calendar_events']['teaserBtnText'] = ['Button-Text'];
$GLOBALS['TL_LANG']['tl_calendar_events']['useAsTeaser'] = ['in der Teaser-Ansicht nutzen'];