<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

  $GLOBALS['TL_LANG']['tl_content']['pleace_select'] = ['bitte wählen', ''];
  $GLOBALS['TL_LANG']['tl_content']['submit_button'] = ['jetzt suchen', ''];

  $GLOBALS['TL_LANG']['tl_content']['subject'] = ['Fachgebiet', ''];
  $GLOBALS['TL_LANG']['tl_content']['disease'] = ['Krankheitsbild', ''];

  // Formular
  $GLOBALS['TL_LANG']['tl_content']['firstname'] = ['Vorname', ''];
  $GLOBALS['TL_LANG']['tl_content']['lastname'] = ['Nachname', ''];
  $GLOBALS['TL_LANG']['tl_content']['email'] = ['E-Mail', ''];
  $GLOBALS['TL_LANG']['tl_content']['phone'] = ['Telefon', ''];
  $GLOBALS['TL_LANG']['tl_content']['comment'] = ['Nachricht', ''];
  $GLOBALS['TL_LANG']['tl_content']['submit'] = 'Nachricht senden';
  $GLOBALS['TL_LANG']['tl_content']['formtitle'] = 'Kontaktformular';
  $GLOBALS['TL_LANG']['tl_content']['showLocation'] = 'Standort anzeigen';
  $GLOBALS['TL_LANG']['tl_content']['gkh_show_hh'] = ['mit HH Symbol davor', ''];
  $GLOBALS['TL_LANG']['tl_content']['title_row1'] = ['Titel (1. Zeile)', ''];
  $GLOBALS['TL_LANG']['tl_content']['title_row2'] = ['Titel (2. Zeile)', ''];

  $GLOBALS['TL_LANG']['tl_content']['error_message'] = 'Es ist ein Fehler aufgetreten';
  $GLOBALS['TL_LANG']['tl_content']['success_message'] = 'Die Email wurde an %s %s %s versendet.';
  $GLOBALS['TL_LANG']['tl_content']['star_notation'] = 'diese Felder müssen ausgefüllt werden';

   $GLOBALS['TL_LANG']['tl_content']['list_legend'] = 'Listenpunkte (optional zum Text)';
