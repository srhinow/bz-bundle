<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['tl_libraries']['libname'] = ['Name der Bibliothek mit Zusatz', 'Geben Sie hier den Namen der Bibliothek an.'];
$GLOBALS['TL_LANG']['tl_libraries']['firstname'] = ['Vorname', 'Vorname der Kontaktperson'];
$GLOBALS['TL_LANG']['tl_libraries']['lastname'] = ['Nachname', 'Nachname der Kontaktperson'];
$GLOBALS['TL_LANG']['tl_libraries']['street'] = ['Straße', 'Bitte geben Sie den Straßennamen und die Hausnummer ein.'];
$GLOBALS['TL_LANG']['tl_libraries']['postal'] = ['Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.'];
$GLOBALS['TL_LANG']['tl_libraries']['city'] = ['Ort', 'Bitte geben Sie den Namen des Ortes ein.'];
$GLOBALS['TL_LANG']['tl_libraries']['state'] = ['Bundesland', 'Hier können Sie die Bibliothek einem Bundesland zuordnen.'];
$GLOBALS['TL_LANG']['tl_libraries']['phone'] = ['Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.'];
$GLOBALS['TL_LANG']['tl_libraries']['email'] = ['E-Mail-Adresse', 'Bitte geben Sie die E-Mail-Adresse des Abonnenten ein.'];
$GLOBALS['TL_LANG']['tl_libraries']['country'] = ['Land', 'Hier können Sie der Bibliothek ein Land zuordnen.'];
$GLOBALS['TL_LANG']['tl_libraries']['state'] = ['Bundesland', 'Hier können Sie die Bibliothek einem Bundesland zuordnen.'];
$GLOBALS['TL_LANG']['tl_libraries']['mobile'] = ['Handynummer', 'Bitte geben Sie die Handynummer ein.'];
$GLOBALS['TL_LANG']['tl_libraries']['fax'] = ['Faxnummer', 'Bitte geben Sie die Faxnummer ein.'];
$GLOBALS['TL_LANG']['tl_libraries']['website'] = ['Webseite', 'Hier können Sie eine Web-Adresse eingeben.'];
$GLOBALS['TL_LANG']['tl_libraries']['memo'] = ['Memo / Notizen', 'Hier können Sie weitere Informationen eingeben.'];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_libraries']['new'] = ['Neue Bibliothek', 'Eine neue Bibliothek anlegen'];
$GLOBALS['TL_LANG']['tl_libraries']['show'] = ['Buchungdetails', 'Details der Bibliothek ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_libraries']['edit'] = ['Bibliothek bearbeiten', 'Bibliothek ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_libraries']['editheader'] = ['Bibliothek-Einstellungen bearbeiten', 'Einstellungen der Bibliothek ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_libraries']['copy'] = ['Bibliothek duplizieren', 'Bibliothek ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_libraries']['cut'] = ['Bibliothek verschieben', 'Bibliothek ID %s verschieben'];
$GLOBALS['TL_LANG']['tl_libraries']['delete'] = ['Bibliothek löschen', 'Bibliothek ID %s löschen'];
$GLOBALS['TL_LANG']['tl_libraries']['toggle'] = ['Bibliothek veröffentlichen/unveröffentlichen', 'Bibliothek ID %s veröffentlichen/unveröffentlichen'];

/*
* Legends
*/
$GLOBALS['TL_LANG']['tl_libraries']['contact_legend'] = 'Kontaktdaten';
