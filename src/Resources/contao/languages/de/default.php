<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['MSC']['tags']['0'] = 'Schlagworte';
$GLOBALS['TL_LANG']['MSC']['tags']['1'] = 'Bitte geben Sie eine oder mehrere Schlagworte durch Komma getrennt an, um eine Kategorisierung vorzunehmen.';
$GLOBALS['TL_LANG']['MSC']['apply'] = 'Aktualisieren';
$GLOBALS['TL_LANG']['MSC']['nl_subject'] = 'Ihre Newsletter-Anmeldung auf %s';

$GLOBALS['TL_LANG']['MSC']['nl_activate'] = 'Die Anmeldung zu unserem Newsletter war erfolgreich.';
$GLOBALS['TL_LANG']['ERR']['subscribed'] = 'Sie sind bereits für unseren Newsletter angemeldet.';
$GLOBALS['TL_LANG']['ERR']['unsubscribed'] = 'Sie sind nicht für unseren Newsletter angemeldet.';
$GLOBALS['TL_LANG']['ERR']['email'] = 'Bitte geben Sie eine gültige E-Mail-Adresse ein.';
