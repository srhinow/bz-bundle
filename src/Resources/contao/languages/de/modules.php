<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['MOD']['libraries'] = ['Bibliotheken'];

/*
 * Content elements
 */
$GLOBALS['TL_LANG']['CTE']['bz'] = ['Büchereizentrale', ''];
$GLOBALS['TL_LANG']['CTE']['bz_borderbox'] = ['umrandeter Inhalt', ''];
$GLOBALS['TL_LANG']['CTE']['bz_subnavi_level0'] = ['Navigation-Punkte (gleiche Ebene)', ''];
$GLOBALS['TL_LANG']['CTE']['bz_subnavi_level1'] = ['Navigation-Unterpunkte (Level 1)', ''];
$GLOBALS['TL_LANG']['CTE']['bz_subnavi_level2'] = ['Navigation-Unterpunkte (Level 2)', ''];
