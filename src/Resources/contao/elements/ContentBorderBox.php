<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/**
 * Class ContentBorderBox.
 */
class ContentBorderBox extends \ContentElement
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'ce_bz_borderbox';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        // $this->tmObj = TeaserModel::findById($this->tm_teaser);

        if (TL_MODE === 'BE') {
            /* @var \BackendTemplate|object $objTemplate */
            // $objTemplate = new \BackendTemplate('be_wildcard');

            // $objTemplate->wildcard = '### Liste "'.$this->title.'" in einer BorderBox ###';
            // $objTemplate->title = $this->headline;

            // return $objTemplate->parse();
        }

        if (\strlen($this->customTpl) > 0) {
            $this->strTemplate = $this->customTpl;
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        // print_r($this->listitems);
        $this->Template->title = $this->title;
        $this->Template->listitems = unserialize($this->listitems);
        $this->Template->text = $this->gkh_text;
    }
}
