<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/**
 * Class ContentMainnavDeep2.
 */
class ContentMainnavDeep1 extends ContentGkh
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'ce_bz_mainnav_deep1';

    protected $navigationTpl = 'nav_gkh_main';

    protected $showLevel = 1;

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            /** @var \BackendTemplate|object $objTemplate */
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### Unternavigationspunkte Tiefe:1 ###';
            $objTemplate->title = $this->headline;

            return $objTemplate->parse();
        }

        if (\strlen($this->customTpl) > 0) {
            $this->strTemplate = $this->customTpl;
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        /* @var \PageModel $objPage */
        global $objPage;

        // $objSubpages = \PageModel::findPublishedSubpagesWithoutGuestsByPid($objPage->id);
        // while($objSubpages->next()) {print "<br>"; print_r($objSubpages);}
        $this->Template->items = $this->renderNavigation($objPage->id, 1, null, null);
    }
}
