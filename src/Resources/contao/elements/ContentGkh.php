<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

class ContentGkh extends \ContentElement
{
    /** Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        return parent::generate();
    }

    /** Display a wildcard in the back end
     *
     * @return string
     */
    protected function compile()
    {
    }

    /**
     * Recursively compile the navigation menu and return it as HTML string.
     *
     * @param int    $pid
     * @param int    $level
     * @param string $host
     * @param string $language
     *
     * @return string
     */
    protected function renderNavigation($pid, $level = 1, $host = null, $language = null)
    {
        // Get all active subpages
        $objSubpages = \PageModel::findPublishedSubpagesWithoutGuestsByPid($pid, $this->showHidden, $this instanceof \ModuleSitemap);

        if (null === $objSubpages) {
            return '';
        }

        $items = [];
        $groups = [];

        // Get all groups of the current front end user
        if (FE_USER_LOGGED_IN) {
            $this->import('FrontendUser', 'User');
            $groups = $this->User->groups;
        }

        // Layout template fallback
        if ('' === $this->navigationTpl) {
            $this->navigationTpl = 'nav_default';
        }

        /** @var \FrontendTemplate|object $objTemplate */
        $objTemplate = new \FrontendTemplate($this->navigationTpl);

        $objTemplate->pid = $pid;
        $objTemplate->type = \get_class($this);
        $objTemplate->cssID = $this->cssID; // see #4897
        $objTemplate->level = 'level_'.$level++;

        /* @var \PageModel $objPage */
        global $objPage;

        // Browse subpages
        while ($objSubpages->next()) {
            // Skip hidden sitemap pages
            if ($this instanceof \ModuleSitemap && 'map_never' === $objSubpages->sitemap) {
                continue;
            }

            $subitems = '';
            $_groups = deserialize($objSubpages->groups);

            // Override the domain (see #3765)
            if (null !== $host) {
                $objSubpages->domain = $host;
            }

            // Do not show protected pages unless a back end or front end user is logged in
            if (!$objSubpages->protected || BE_USER_LOGGED_IN || (\is_array($_groups) && \count(array_intersect($_groups, $groups))) || $this->showProtected || ($this instanceof \ModuleSitemap && 'map_always' === $objSubpages->sitemap)) {
                // Check whether there will be subpages
                if ($objSubpages->subpages > 0 && (!$this->showLevel || $this->showLevel >= $level || (!$this->hardLimit && ($objPage->id === $objSubpages->id || \in_array($objPage->id, $this->Database->getChildRecords($objSubpages->id, 'tl_page'), true))))) {
                    $subitems = $this->renderNavigation($objSubpages->id, $level, $host, $language);
                }

                $href = null;

                // Get href
                switch ($objSubpages->type) {
                    case 'redirect':
                        $href = $objSubpages->url;

                        if (0 === strncasecmp($href, 'mailto:', 7)) {
                            $href = \StringUtil::encodeEmail($href);
                        }
                        break;

                    case 'forward':
                        if ($objSubpages->jumpTo) {
                            /** @var \PageModel $objNext */
                            $objNext = $objSubpages->getRelated('jumpTo');
                        } else {
                            $objNext = \PageModel::findFirstPublishedRegularByPid($objSubpages->id);
                        }

                        if (null !== $objNext) {
                            // Hide the link if the target page is invisible
                            if (!$objNext->published || ('' !== $objNext->start && $objNext->start > time()) || ('' !== $objNext->stop && $objNext->stop < time())) {
                                continue 2;
                            }

                            $strForceLang = null;
                            $objNext->loadDetails();

                            // Check the target page language (see #4706)
                            if (\Config::get('addLanguageToUrl')) {
                                $strForceLang = $objNext->language;
                            }

                            $href = $this->generateFrontendUrl($objNext->row(), null, $strForceLang, true);
                            break;
                        }
                        // DO NOT ADD A break; STATEMENT

                        // no break
                    default:
                        if ('' !== $objSubpages->domain && $objSubpages->domain !== \Environment::get('host')) {
                            /** @var \PageModel $objModel */
                            $objModel = $objSubpages->current();
                            $objModel->loadDetails();
                        }

                        $href = $this->generateFrontendUrl($objSubpages->row(), null, $language, true);
                        break;
                }

                $row = $objSubpages->row();
                $trail = \in_array($objSubpages->id, $objPage->trail, true);

                // Active page
                if (($objPage->id === $objSubpages->id || 'forward' === $objSubpages->type && $objPage->id === $objSubpages->jumpTo) && !$this instanceof \ModuleSitemap && $href === \Environment::get('request')) {
                    // Mark active forward pages (see #4822)
                    $strClass = (('forward' === $objSubpages->type && $objPage->id === $objSubpages->jumpTo) ? 'forward'.($trail ? ' trail' : '') : 'active').(('' !== $subitems) ? ' submenu' : '').($objSubpages->protected ? ' protected' : '').(('' !== $objSubpages->cssClass) ? ' '.$objSubpages->cssClass : '');

                    $row['isActive'] = true;
                    $row['isTrail'] = false;
                }

                // Regular page
                else {
                    $strClass = (('' !== $subitems) ? 'submenu' : '').($objSubpages->protected ? ' protected' : '').($trail ? ' trail' : '').(('' !== $objSubpages->cssClass) ? ' '.$objSubpages->cssClass : '');

                    // Mark pages on the same level (see #2419)
                    if ($objSubpages->pid === $objPage->pid) {
                        $strClass .= ' sibling';
                    }

                    $row['isActive'] = false;
                    $row['isTrail'] = $trail;
                }

                $row['subitems'] = $subitems;
                $row['class'] = trim($strClass);
                $row['title'] = specialchars($objSubpages->title, true);
                $row['pageTitle'] = specialchars($objSubpages->pageTitle, true);
                $row['link'] = $objSubpages->title;
                $row['href'] = $href;
                $row['nofollow'] = (0 === strncmp($objSubpages->robots, 'noindex', 7));
                $row['target'] = '';
                if(null !== $objSubpages->description) {
                    $row['description'] = str_replace(
                        ["\n", "\r"],
                        [' ', ''],
                        $objSubpages->description
                    );
                }

                // Override the link target
                if ('redirect' === $objSubpages->type && $objSubpages->target) {
                    $row['target'] = ('xhtml' === $objPage->outputFormat) ? ' onclick="return !window.open(this.href)"' : ' target="_blank"';
                }

                $items[] = $row;
            }
        }

        // Add classes first and last
        if (!empty($items)) {
            $last = \count($items) - 1;

            $items[0]['class'] = trim($items[0]['class'].' first');
            $items[$last]['class'] = trim($items[$last]['class'].' last');
        }

        $objTemplate->items = $items;

        return !empty($items) ? $objTemplate->parse() : '';
    }
}
