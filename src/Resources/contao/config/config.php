<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------.
 */
array_insert($GLOBALS['BE_MOD']['accounts'], 0, [
    'libraries' => [
        'tables' => ['tl_libraries'],
    ],
]
);

$GLOBALS['BE_MOD']['accounts']['member']['csvMemberExport'] = ['srhinow.bz_bundle.service.csv_export', 'csvMemberExport'];

/*
 * -------------------------------------------------------------------------
 * Add content element
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['TL_CTE'], 1, ['bz' => []]);

$GLOBALS['TL_CTE']['bz']['bz_borderbox'] = 'ContentBorderBox';
$GLOBALS['TL_CTE']['bz']['bz_subnavi_level0'] = 'ContentMainnavDeep0';
$GLOBALS['TL_CTE']['bz']['bz_subnavi_level1'] = 'ContentMainnavDeep1';
$GLOBALS['TL_CTE']['bz']['bz_subnavi_level2'] = 'ContentMainnavDeep2';

/*
 * -------------------------------------------------------------------------
 * MODELS
 * -------------------------------------------------------------------------
 */

$GLOBALS['TL_MODELS']['tl_libraries'] = \Srhinow\BzBundle\Model\LibrariesModel::class;

/*
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 * wird per ServiceAnnotation in der jeweiligen Hook-Class geladen
 */

//$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('bzHooks', 'bzNewsReplaceInsertTags');
//$GLOBALS['TL_HOOKS']['activateRecipient'][] = array('bzHooks', 'activateNewsletterRecipient');
//$GLOBALS['TL_HOOKS']['removeRecipient'][] = array('bzHooks', 'removeNewsletterRecipient');

/*
 * -------------------------------------------------------------------------
 * sonstige configs
 * -------------------------------------------------------------------------
 */
$GLOBALS['bz_states'] = [
    '1' => 'Bayern',
    '2' => 'Baden-Württemberg',
    '3' => 'Rheinland-Pfalz',
    '4' => 'Mecklenburg-Vorpommern',
    '5' => 'Sachsen-Anhalt',
    '6' => 'Brandenburg',
    '7' => 'Niedersachsen',
    '8' => 'Schleswig-Holstein',
    '9' => 'Nordrhein-Westfalen',
    '10' => 'Thüringen',
    '11' => 'Hessen',
    '12' => 'Sachsen',
    '13' => 'Berlin',
    '14' => 'Saarland',
    '15' => 'Bremen',
    '16' => 'Hamburg',
];
