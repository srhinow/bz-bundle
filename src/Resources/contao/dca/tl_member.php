<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Table tl_member.
 */

use Srhinow\BzBundle\Helper\BzHelper;

$GLOBALS['TL_DCA']['tl_member']['list']['label']['fields'] = ['icon', 'library_name', 'lastname', 'username'];

/*
* global Operations
*/
array_insert($GLOBALS['TL_DCA']['tl_member']['list']['global_operations'], 0, [
    'csvMemberExport' => [
        'label' => &$GLOBALS['TL_LANG']['tl_member']['csvMemberExport'],
        'href' => 'key=csvMemberExport',
        'class' => 'export_csv',
        'attributes' => 'onclick="Backend.getScrollOffset();"',
    ],
]
);
// Palettes
$GLOBALS['TL_DCA']['tl_member']['palettes'] = [
    '__selector__' => ['login'],
    'default' => '{address_legend},library_id,street,postal,city,state,country;{groups_legend},groups;{personal_legend},firstname,lastname,job_position;{contact_legend},phone,mobile,fax,email,website,language;{login_legend},login;{account_legend},disable,start,stop',
];

// Fields
$GLOBALS['TL_DCA']['tl_member']['fields']['firstname']['eval']['mandatory'] = false;
$GLOBALS['TL_DCA']['tl_member']['fields']['lastname']['eval']['mandatory'] = false;
// unset($GLOBALS['TL_DCA']['tl_member']['fields']['company']);
$GLOBALS['TL_DCA']['tl_member']['fields']['street']['eval']['tl_class'] = 'long';
$GLOBALS['TL_DCA']['tl_member']['fields']['country']['default'] = 'de';
$GLOBALS['TL_DCA']['tl_member']['fields']['language']['default'] = 'de';

$GLOBALS['TL_DCA']['tl_member']['fields']['library_id'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_member']['library_id'],
    'exclude' => false,
    'filter' => true,
    'sorting' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow.bz_bundle.listeners.dca.member', 'getLibraryOptions'],
    'save_callback' => [
        ['srhinow.bz_bundle.listeners.dca.member', 'fillLibraryFields'],
    ],
    'eval' => ['includeBlankOption' => true, 'chosen' => true, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'address', 'tl_class' => 'long'],
    'sql' => "int(10) unsigned NOT NULL default '0'",
];
$GLOBALS['TL_DCA']['tl_member']['fields']['library_name'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_member']['library_name'],
    'exclude' => true,
    'search' => true,
    'sorting' => true,
    'flag' => 11,
    'inputType' => 'text',
    'eval' => ['feEditable' => true, 'feViewable' => true, 'feGroup' => 'address', 'tl_class' => 'clr long'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_member']['fields']['state'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_member']['state'],
    'exclude' => true,
    'filter' => true,
    'sorting' => true,
    'flag' => 11,
    'inputType' => 'select',
    'options' => BzHelper::getStatesAsOptions(),
    'eval' => ['includeBlankOption' => true, 'chosen' => true, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'address', 'tl_class' => 'w50'],
    'sql' => "varchar(64) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_member']['fields']['job_position'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_member']['job_position'],
    'exclude' => true,
    'search' => true,
    'filter' => true,
    'sorting' => true,
    'flag' => 11,
    'inputType' => 'select',
    'options' => &$GLOBALS['TL_LANG']['tl_member']['job_position_options'],
    'eval' => ['maxlength' => 255, 'decodeEntities' => true, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'contact', 'tl_class' => 'clr long'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_member']['fields']['library_name'] = [
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_member']['fields']['import_from'] = [
    'sql' => "varchar(255) NOT NULL default ''",
];
