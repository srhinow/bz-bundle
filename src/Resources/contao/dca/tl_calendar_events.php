<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default'] = str_replace(
    ',teaser',
    ',teaser,details',
    $GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default']
);
$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default'] .= ';{teaser_legend},teaserImageSRC,teaserTitle,teaserSubTitle,teaserBtnText,useAsTeaser';

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['details'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_calendar_events']['details'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['rte' => 'tinyMCE', 'tl_class' => 'clr'],
    'sql' => 'text NULL',
];
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['teaserImageSRC'] = [
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array(
        'filesOnly'=>true,
        'fieldType'=>'radio',
        'extensions'=>Contao\Config::get('validImageTypes')
    ),
    'sql'                     => "binary(16) NULL"
];
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['teaserTitle'] = [
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['teaserSubTitle'] = [
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['teaserBtnText'] = [
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'default'                 => 'Entdecken',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default 'Entdecken'"
];
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['useAsTeaser'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'clr w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
];

