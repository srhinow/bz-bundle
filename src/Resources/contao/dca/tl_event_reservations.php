<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_event_reservations']['fields']['state'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['state'],
    'exclude' => true,
    'filter' => true,
    'search' => true,
    'sorting' => true,
    'inputType' => 'select',
    'options' => \Srhinow\BzBundle\Helper\BzHelper::getStatesAsOptions(),
    'eval' => ['maxlength' => 255, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'address', 'tl_class' => 'w50'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_event_reservations']['fields']['library'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['library'],
    'exclude' => true,
    'search' => true,
    'sorting' => true,
    'flag' => 1,
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'maxlength' => 255, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'personal', 'tl_class' => 'w50'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_event_reservations']['fields']['libtype'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['libtype'],
    'exclude' => true,
    'filter' => true,
    'search' => true,
    'sorting' => true,
    'inputType' => 'select',
    'options' => &$GLOBALS['TL_LANG']['tl_event_reservations']['libtype_options'],
    'eval' => ['maxlength' => 255, 'feGroup' => 'address', 'tl_class' => 'w50'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_event_reservations']['fileds']['palettes'] = [
    'default' => '
        {main_legend},firstname,lastname,library,libtype,street,postal,city,state;
        {contact_legend},phone,email;accepted;subject,html_email,text_email;sendEmail
    ',
];
