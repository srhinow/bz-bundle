<?php
$GLOBALS['TL_DCA']['tl_module']['palettes']['eventlist'] .= ';{teaser_legend},useOnlyAsTeaser,contentBefore,contentAfter';

$GLOBALS['TL_DCA']['tl_module']['fields']['useOnlyAsTeaser'] = array
(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['contentBefore'] = [
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['style' => 'height:60px;', 'tl_class' => 'clr'],
    'sql' => 'text NULL',
];
$GLOBALS['TL_DCA']['tl_module']['fields']['contentAfter'] = [
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['style' => 'height:60px;', 'tl_class' => 'clr'],
    'sql' => 'text NULL',
];