<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\BzBundle\Helper;

class BzHelper
{
    public static function getStatesAsOptions()
    {
        $return = [];

        foreach ($GLOBALS['bz_states'] as $k => $state) {
            $return[$k] = $state;
        }

        return $return;
    }
}
