<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */
namespace Srhinow\BzBundle\Service;

use Contao\Backend;
use Contao\File;
use Contao\StringUtil;

/**
 * Class iao_invoice.
 */
class BeCsvExport extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Export bookings for statistics.
     */
    public function csvMemberExport()
    {
        $this->import('Session');

        $sessionFilter = $this->Session->get('filter');

        if ('export_member' === $this->Input->post('FORM_SUBMIT')) {
            $this->import('Database');

            //set handle from file
            $seperators = ['comma' => ',', 'semicolon' => ';', 'tabulator' => "\t", 'linebreak' => "\n"];
            $listFields = $this->Database->listFields('tl_member');
            $fieldnames = [];
            $ignorefields = ['dateOfBirth', 'company', 'mobile', 'website', 'language', 'PRIMARY', 'autologin', 'assignDir', 'homeDir', 'loginCount', 'locked', 'session', 'currentLogin', 'activation','password','loginAttempts','secret','useTwoFactor','backupCodes','tags','trustedTokenVersion'];
//            $ignorefields = array();

            foreach ($listFields as $k => $field) {
                if (\in_array($field['name'], $ignorefields, true) || 'index' === $field['type']) {
                    continue;
                }
                $fieldnames[] = $field['name'];
            }

            // Gruppen
            $groupObj = $this->Database->prepare('SELECT id,name FROM `tl_member_group`')->execute();
            $groupsArr = $groupObj->fetchAllAssoc();
            reset($groupsArr);
            foreach ($groupsArr as $k => $group) {
                $groupfields[$group['id']] = standardize($group['name']);
                $headGroupNames[] = strip_tags('Gruppe: '.$group['name']);
            }

            //statename mit aufnehmen
            $headnames = array_merge($this->getHeaderFieldNames($fieldnames), $headGroupNames);

            // get records
            $arrExport = $selectfields = [];

            foreach ($fieldnames as $fieldname) {
                $sqlfields[] = 'tl_member.'.$fieldname;
            }

            $selectfields = implode(', ', $sqlfields);

            $memObj = $this->Database->prepare('SELECT '.$selectfields.',state FROM `tl_member` ORDER BY `library_name` ASC')->execute();

            if ($memObj->numRows < 1) {
                $_SESSION['TL_ERROR'][] = 'keine Daten zum exportieren vorhanden';
                $this->reload();
            }

            while ($memObj->next()) {

                $curgroups = (null === $memObj->groups)?[]:unserialize($memObj->groups);
                if (!\is_array($curgroups)) {
                    $curgroups = [];
                }

                //Filter anwenden
                if (isset($sessionFilter['tl_member']['groups']) && !\in_array($sessionFilter['tl_member']['groups'], $curgroups, true)) {
                    continue;
                }

                //Gruppen zugehoerigkeiten des Mitgliedes als Mitglied-eigenschaft setzen
                foreach ($groupfields as $gid => $gname) {
                    $memObj->{$groupfields[$gid]} = \in_array((string) $gid, $curgroups, true) ? 'ja' : 'nein';
                }

                //state - Zuweisung
                //		    	if(is_numeric($memObj->state)) $memObj->state = $memObj->stname;

                $arrExport = $memObj->row();
                $arrExport['state'] = $GLOBALS['bz_states'][$arrExport['state']] ?? $arrExport['state'];

                $arrExports[] = $arrExport;
            }

            // start output
            $exportFile = 'contao_member_'.date('Ymd-Hi');

            $output = '"'.implode('"'.$seperators[$this->Input->post('separator')].'"', array_values($headnames)).'"'."\n";

            foreach ($arrExports as $export) {
                $output .= '"'.implode('"'.$seperators[$this->Input->post('separator')].'"', str_replace('"', '""', $export)).'"'."\n";
            }
            ob_end_clean();
            header('Content-Type: application/csv');
            header('Content-Transfer-Encoding: binary');
            header('Content-Disposition: attachment; filename="'.$exportFile.'.csv"');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Expires: 0');
            echo $output;
            exit();
        }

        // Return the form
        return '
		    <div id="tl_buttons">
		    <a href="'.ampersand(str_replace('&key=csvMemberExport', '', $this->Environment->request)).'" class="header_back" title="'.StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['backBT']).'" accesskey="b">'.$GLOBALS['TL_LANG']['MSC']['backBT'].'</a>
		    </div>

		    <h2 class="sub_headline">'.$GLOBALS['TL_LANG']['tl_member']['h2_exportcsv'].'</h2>'.$this->getMessages().'

		    <form action="'.ampersand($this->Environment->request, true).'" id="tl_bbk_csvexport" class="tl_form" method="post">
		    <div class="tl_formbody_edit">
			<input type="hidden" name="FORM_SUBMIT" value="export_member" />
			<input type="hidden" name="REQUEST_TOKEN" value="'.REQUEST_TOKEN.'" />
			<input type="hidden" name="pid" value="'.$this->Input->get('id').'" />
			<fieldset class="tl_tbox block">
			    <div class="w50">
			    <h3><label for="ctrl_bbk">CSV-Trenner</label></h3>
			    <select name="separator" id="separator" class="tl_select" onfocus="Backend.getScrollOffset();">
				<option value="semicolon">'.$GLOBALS['TL_LANG']['MSC']['semicolon'].' (;)</option>
				<option value="comma">'.$GLOBALS['TL_LANG']['MSC']['comma'].' (,)</option>
				<option value="tabulator">'.$GLOBALS['TL_LANG']['MSC']['tabulator'].'</option>
				<option value="linebreak">'.$GLOBALS['TL_LANG']['MSC']['linebreak'].'</option>
			    </select>'.(('' !== $GLOBALS['TL_LANG']['MSC']['separator'][1]) ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['MSC']['separator'][1].'</p>' : '').'
			    </div>
			</fieldset>
		    </div>

		    <div class="tl_formbody_submit">

		    <div class="tl_submit_container">
		      <input type="submit" name="save" id="save" class="tl_submit" accesskey="s" value="'.StringUtil::specialchars($GLOBALS['TL_LANG']['tl_member']['button_exportcsv']).'" />
		    </div>

		    </div>
		    </form>';
    }

    private function getHeaderFieldNames(array $arrFields): array
    {
        $replaces = ['state' => 'Bundesland'];

        $arrReturn = [];
        foreach ($arrFields as $field) {
            $arrReturn[] = isset($replaces[$field])?$replaces[$field]:$field;
        }

        return $arrReturn;
    }
    /**
     * generate runlist.
     */
    public function runList()
    {
        if ('generate_runlist' === $this->Input->post('FORM_SUBMIT')) {
            $this->import('Database');

            $pid = $this->Input->post('pid');

            //Check the reference-id
            if ($pid < 1) {
                $_SESSION['TL_ERROR'][] = 'keine ID als Daten-Referenz angegeben';
                $this->reload();
            }

            $resultObj = $this->Database->prepare('SELECT `tl_bbk`.*,
		    `tl_bbk_locations`.`name` as `standort`,
		    `tl_bbk_booking`.`startDate`,
		    `tl_bbk_booking`.`endDate`,
		    `tl_bbk_booking`.`firstname`,
		    `tl_bbk_booking`.`lastname`,
		    `tl_bbk_booking`.`library`,
		    `tl_bbk_booking`.`street`,
		    `tl_bbk_booking`.`postal`,
		    `tl_bbk_booking`.`city`,
		    `tl_bbk_booking`.`phone`,
		    `tl_bbk_booking`.`email`
		    FROM `tl_bbk_booking`
		    LEFT JOIN `tl_bbk` ON `tl_bbk_booking`.`bbk` = `tl_bbk`.`id`
		    LEFT JOIN `tl_bbk_locations` ON `tl_bbk_booking`.`pid` = `tl_bbk_locations`.`id`
		    WHERE `tl_bbk_locations`.`id` = ?
		    AND (`startDate` >= '.strtotime($this->Input->post('beginnDate')).' && `startDate` <= '.strtotime($this->Input->post('endDate')).')
		    AND `accepted` != 2
		    ORDER BY `startDate` ASC')
                ->execute($pid)
            ;

            //Check the reference-id
            if ($resultObj->numRows < 1) {
                $_SESSION['TL_ERROR'][] = 'keine Daten zum exportieren vorhanden';
                $this->reload();
            }
            $arrExport = $resultObj->fetchAllAssoc();

            $fieldnames = ['bookingtime' => 'Zeitraum', 'standort' => 'Standort', 'bbk' => 'BBK', 'adresse' => 'Adresse'];

            // start output
            $exportFile = 'export_'.$resultObj->alias.'_'.date('Ymd-Hi');
            $output = $head = '';
            foreach (array_values($fieldnames) as $field) {
                $head .= '<th>'.$field.'</th>';
            }
            $output = '<thead><tr>'.$head.'</tr></thead>';

            foreach ($arrExport as $export) {
                $row = '';
                $rowArr = [
                    'bookingtime' => 'Beginn:'.date($GLOBALS['TL_CONFIG']['dateFormat'], $export['startDate']).'<br>Ende: '.date($GLOBALS['TL_CONFIG']['dateFormat'], $export['endDate']),
                    'standort' => $export['standort'],
                    'bbk' => '<b>BBK-Nummer:</b> '.$export['bbkNr'].'<br><b>BBK-Titel:</b> '.$export['title'].'<br><b>BBK-Typ:</b> '.$export['mediatyp'].'<br>',
                    'adresse' => '<b>Adresse:</b> '.$export['library'].'<br>'.$export['firstname'].' '.$export['lastname'].'<br>'.$export['street'].'<br>'.$export['postal'].' '.$export['city']
                        .'<br><b>Kontakt:</b> <br>Telefon:'.$export['phone'].'<br>Email: '.$export['email'].'<br>',
                ];

                foreach ($rowArr as $k => $cell) {
                    $row .= '<td>'.$cell.'</td>';
                }
                $output .= '<tr>'.$row.'</tr>';
            }
            $output = '<table>'.$output.'</table>';

            $pdfFile = new File('tl_files/bz-niedersachsen/css/pdf.css');
            $cssText = $pdfFile->getContent();
            $cssText = '<style>'.$cssText.'</style>';

            ob_end_clean();
            $this->createPdf($cssText.$output);
        }

        $beginnDate = ($this->Input->post('beginnDate')) ? $this->Input->post('beginnDate') : date($GLOBALS['TL_CONFIG']['dateFormat'], strtotime('+14 days'));
        $endDate = ($this->Input->post('endDate')) ? $this->Input->post('endDate') : date($GLOBALS['TL_CONFIG']['dateFormat'], strtotime('+21 days'));
        // Return the form
        return '
		    <div id="tl_buttons">
		    <a href="'.ampersand(str_replace('&key=runList', '', $this->Environment->request)).'" class="header_back" title="'.specialchars($GLOBALS['TL_LANG']['MSC']['backBT']).'" accesskey="b">'.$GLOBALS['TL_LANG']['MSC']['backBT'].'</a>
		    </div>

		    <h2 class="sub_headline">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList'][1].'</h2>'.$this->getMessages().'

		    <form action="'.ampersand($this->Environment->request, true).'" id="tl_bbk_csvexport" class="tl_form" method="post">
		    <div class="tl_formbody_edit">
			<input type="hidden" name="FORM_SUBMIT" value="generate_runlist" />
			<input type="hidden" name="REQUEST_TOKEN" value="'.REQUEST_TOKEN.'" />
			<input type="hidden" name="pid" value="'.$this->Input->get('id').'" />
			<fieldset class="tl_tbox block">
			    <div class="w50">
			    <h3><label for="ctrl_bbk">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_beginnDate'][0].'</label></h3>
			    <input type="text" name="beginnDate" id="ctrl_beginnDate" value='.$beginnDate.'>
			    '.(('' !== $GLOBALS['TL_LANG']['MSC']['separator'][1]) ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_beginnDate'][1].'</p>' : '').'
			    </div>
			    <div class="w50">
			    <h3><label for="ctrl_endDate">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_endDate'][0].'</label></h3>
			    <input type="text" name="endDate" id="ctrl_endDate" value='.$endDate.'>
			    '.(('' !== $GLOBALS['TL_LANG']['MSC']['separator'][1]) ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_endDate'][1].'</p>' : '').'
			    </div>
			</fieldset>
		    </div>

		    <div class="tl_formbody_submit">

		    <div class="tl_submit_container">
		      <input type="submit" name="save" id="save" class="tl_submit" accesskey="s" value="'.specialchars($GLOBALS['TL_LANG']['tl_bbk_booking']['runlist_generate_button'][0]).'" />
		    </div>

		    </div>
		    </form>';
    }

    /**
     * Print an article as PDF and stream it to the browser.
     *
     * @param Database_Result
     */
    protected function createPdf($strContent = ''): void
    {
        // TCPDF configuration
        $l['a_meta_dir'] = 'ltr';
        $l['a_meta_charset'] = $GLOBALS['TL_CONFIG']['characterSet'];
        $l['a_meta_language'] = $GLOBALS['TL_LANGUAGE'];
        $l['w_page'] = 'page';

        // Include library
        require_once TL_ROOT.'/system/config/tcpdf.php';
        require_once TL_ROOT.'/plugins/tcpdf/tcpdf.php';

        // Create new PDF document
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);

        $title = date('Ymdhis').'-runlist';

        // Set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(PDF_AUTHOR);
        $pdf->SetTitle($title);
        $pdf->SetSubject($title);
        $pdf->SetKeywords($title);

        // Prevent font subsetting (huge speed improvement)
        $pdf->setFontSubsetting(false);

        // Remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // Set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // Set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Set some language-dependent strings
        $pdf->setLanguageArray($l);

        // Initialize document and add a page
        $pdf->AddPage();

        // Set font
        $pdf->SetFont(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN);

        // Write the HTML content
        $pdf->writeHTML($strContent, true, 0, true, 0);

        // Close and output PDF document
        $pdf->lastPage();
        $pdf->Output(standardize(ampersand($title, false)).'.pdf', 'D');

        // Stop script execution
        exit;
    }
}
