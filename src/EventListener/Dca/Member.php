<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\BzBundle\EventListener\Dca;

use Contao\MemberModel;
use Srhinow\BzBundle\Model\LibrariesModel;

class Member
{
    /**
     * get custom view from library-item-options.
     *
     * @param object
     */
    public function getLibraryOptions()
    {
        $arrLibraries = [];
        $objLibraries = LibrariesModel::findAll(['order' => 'city ASC']);

        while ($objLibraries->next()) {
            $arrLibraries[$objLibraries->id] = $objLibraries->postal.' '.$objLibraries->city.' ('.$objLibraries->libname.') '.$objLibraries->lastname;
        }

        return $arrLibraries;
    }

    /**
     * fill library fields in member-table.
     *
     * @param object
     * @param mixed|null $dc
     *
     * @throws \Exception
     */
    public function fillLibraryFields($varValue, $dc = null)
    {
        if (TL_MODE !== 'BE') {
            return $varValue;
        }

        if (\strlen($varValue) <= 0) {
            return $varValue;
        }

        if(null === ($objLibrary = LibrariesModel::findByPk($varValue))) {
            return $varValue;
        }

        if(null === ($objMember = MemberModel::findByPk($dc->id))) {
            return $varValue;
        }

        if ($objMember->library_name !== $objLibrary->library_name) {
            $objMember->library_name = $objLibrary->libname;
        }
        if ($objMember->postal !== $objLibrary->postal) {
            $objMember->postal = $objLibrary->postal;
        }
        if ($objMember->city !== $objLibrary->city) {
            $objMember->city = $objLibrary->city;
        }
        if ($objMember->street !== $objLibrary->street) {
            $objMember->street = $objLibrary->street;
        }
        if ($objMember->state !== $objLibrary->state) {
            $objMember->state = $objLibrary->state;
        }
        if ($objMember->country !== $objLibrary->country) {
            $objMember->country = $objLibrary->country;
        }

        $objMember->save();

        return $varValue;
    }
}
