<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\BzBundle\EventListener\Hook;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Input;
use Contao\NewsModel;

/**
 * @Hook("replaceInsertTags")
 */
class ReplaceInsertTagsListener
{
    public function __invoke(
        string $insertTag,
        bool $useCache,
        string $cachedValue,
        array $flags,
        array $tags,
        array $cache,
        int $_rit,
        int $_cnt
    ) {
        if ('bznews::' === substr($insertTag, 0, 8)) {
            global $objPage;

            // Set the item from the auto_item parameter
            if (!isset($_GET['items']) && \Config::get('useAutoItem') && isset($_GET['auto_item'])) {
                Input::setGet('items', Input::get('auto_item'));
            }

            $split = explode('::', $insertTag);

            // Do not index or cache the page if there are no archives
            if (!Input::get('items')) {
                return $objPage->$split[2];
            }

            // Get the news item
            if (null !== ($objNews = NewsModel::findByIdOrAlias(Input::get('items')))) {
                return $objNews->{$split[1]};
            }

            // return headline as fallback
            return $objNews->headline;
        }

        return false;
    }
}
