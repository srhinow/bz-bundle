<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 08.04.21
 */

namespace Srhinow\BzBundle\EventListener\Hook;


use Contao\Module;

class GetAllEventsListener
{
    public function __invoke(array $events, array $calendars, int $timeStart, int $timeEnd, Module $module): array
    {
        if (!is_array($events) || count($events) < 0) {
            return $events;
        }

        if("1" === $module->useOnlyAsTeaser) {
            return $this->filterTeaserEvents($events, $module);
        }

        return $events;
    }

    public function filterTeaserEvents($events,$module)
    {
        $c = 0;
        $filterEvents = [];
        ksort($events);

        foreach($events as $date1 => $array1) {
            foreach ($array1 as $date2 => $array2) {
                foreach($array2 as $k => $data) {
                    if('1' !== $data['useAsTeaser']) {
                        continue;
                    }

                    $filterEvents[$date1][$date2][$k] = $data;
                    $c++;

                    if($c >= $module->cal_limit) {
                        return $filterEvents;
                    }
                }
            }
        }

        return $filterEvents;
    }
}