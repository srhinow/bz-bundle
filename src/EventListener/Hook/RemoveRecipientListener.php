<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\BzBundle\EventListener\Hook;

use Contao\CoreBundle\ServiceAnnotation\Hook;

/**
 * @Hook("removeRecipient")
 */
class RemoveRecipientListener
{
    public function __invoke(string $email, array $channels): void
    {
        // set custom subject
        $GLOBALS['TL_LANG']['MSC']['nl_subject'] = 'Ihre Abmeldung auf %s';
    }
}
