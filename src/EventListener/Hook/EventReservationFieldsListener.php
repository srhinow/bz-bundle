<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\BzBundle\EventListener\Hook;

use Contao\FrontendUser;

class EventReservationFieldsListener
{
    public function __invoke(array $arrFields): array
    {
        $objUser = FrontendUser::getInstance();
        if (null !== $objUser && isset($arrFields['library'])) {
            $arrFields['library']['value'] = $objUser->library_name;
        }

        return $arrFields;
    }
}
