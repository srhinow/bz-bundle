<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bz-bundle. Customs for bz-niedersachsen.de
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\BzBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Adds the bundle services to the container.
 */
class SrhinowBzBundleExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $mergedConfig, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

//        $loader->load('services.yml');
        $loader->load('listener.yml');
        $loader->load('services.yml');

        //Setzen von globalen Variablen
//        $container->setParameter('bn.testvalue','lorem ipsum...');
    }
}
