<?php
/**
 * Created by flegl-mandantenportal.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 24.11.19
 */

class RoboFile extends \Robo\Tasks
{

    /**
     * Ruft ale Tests auf
     */
    public function runTests(): void
    {
//        $this->runMyCommand('./tools/phpcpd ../src');
        $this->runMyCommand('./tools/php-cs-fixer --config=../php.cs.dist.php fix');
//        $this->runMyCommand('./tools/phpcs --colors --standard=PSR2 --ignore=../src/Resources/public/ ../src');
    }

    /**
     * Fuehrt ein Kommando aus
     * @param $command
     */
    protected function runMyCommand($command): void
    {
        if(!$this->taskexec($command)->run()->wasSuccessful()){
            exit(1);
        }

        $this->say('-------------------------------------------');
    }
}